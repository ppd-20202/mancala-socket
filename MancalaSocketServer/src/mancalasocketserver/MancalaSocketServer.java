/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mancalasocketserver;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.PLAIN_MESSAGE;
import static javax.swing.JOptionPane.showInputDialog;
import static javax.swing.JOptionPane.showMessageDialog;

/**
 *
 * @author thiag
 */
public class MancalaSocketServer {

    static Socket socket;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        ArrayList<PrintStream> players = new ArrayList<>();
        
        try{
            Scanner input = new Scanner(System.in);
            int port = 0;
            boolean portConfigurationError = false;
            
            do {
                try {
                    port = Integer.parseInt(showInputDialog(null, "Digite a porta de acesso entre 1000 e 60000", "", PLAIN_MESSAGE));
                } catch (NumberFormatException e) {
                  portConfigurationError = true;
                }
                portConfigurationError = port < 1000 || port > 60000;
                if(portConfigurationError){
                    showMessageDialog(null, "Digite um número de porta válido entre 1000 e 60000", "", ERROR_MESSAGE);
                }
            } while (portConfigurationError);
            
            
            ServerSocket server = new ServerSocket(port);
            
            System.out.println("Serviço rodando na porta " + port);
            
            socket = server.accept(); //espera o player1 conectar ao socket
            PrintStream player1 = new PrintStream(socket.getOutputStream());
            Mensagem mensageFromPlayer1ToPlayer2 = new Mensagem(socket);

            socket = server.accept(); //espera o player 2 se conectar ao socket
            PrintStream player2 = new PrintStream(socket.getOutputStream());
            mensageFromPlayer1ToPlayer2.setPlayerToSend(player2);
            Mensagem mensageFromPlayer2ToPlayer1 = new Mensagem(socket);
            mensageFromPlayer2ToPlayer1.setPlayerToSend(player1);

            
        }catch (IOException e){
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mancalasocketclient;


import View.TelaPrincipal;
import java.io.IOException;
import java.net.Socket;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.PLAIN_MESSAGE;
import static javax.swing.JOptionPane.showInputDialog;
import static javax.swing.JOptionPane.showMessageDialog;



/**
 *
 * @author thiag
 */
public class MancalaSocketClient {

     /**
     * @param args the command line arguments
     */
    
    public static Socket socket = null;
    public static void main(String[] args) {
        // TODO code application logic here

        String playerName = "";

        String address = "127.0.0.1";
        int port = 5000;
        

        do {
            playerName = showInputDialog(null, "Digite seu nome", "", PLAIN_MESSAGE);
        } while ("!!!@@@###$$$!@#$".equals(playerName) || playerName.isEmpty());
        do {
            try {
                address = showInputDialog(null, "Digite o endereço do servidor", "", PLAIN_MESSAGE);
                port = Integer.parseInt(showInputDialog(null, "Digite a porta de acesso", "", PLAIN_MESSAGE));
            } catch (NumberFormatException e) {
                showMessageDialog(null, "Digite um endereço e um número de porta válidos", "", ERROR_MESSAGE);
            }
        } while (validateConection(address, port));
        
        TelaPrincipal tela = new TelaPrincipal(playerName);
        tela.setVisible(true);
        tela.setSize(725, 525);
        tela.setResizable(false);
    }

    public static boolean validateConection(String address, int port) {
        try {
            MancalaSocketClient.socket = new Socket(address, port);
            System.out.println("Conectado ao servidor");

            if (socket.isConnected()) {
                return false;
            }

        } catch (IOException e) {
            showMessageDialog(null, "Não conseguiu se conectar ao servidor", "", ERROR_MESSAGE);
            return true;
        }
        return true;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

/**
 *
 * @author thiag
 */
public interface Player {
    
    void opponetWantReset();
    void changeName();
    void opponetGiveUp();
    void beginTurn(String holes[]);
    void sendName(String opponetName);
    void endOfGame(String result);
    
}
